﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Announcement : MonoBehaviour {

	public void setAttributes(string text, Color c) {
		transform.Find ("Text").GetComponent<TextMesh> ().text = text;

		transform.Find ("Text/Text (1)").GetComponent<TextMesh> ().text = text;
		transform.Find ("Text/Text (2)").GetComponent<TextMesh> ().text = text;
		transform.Find ("Text/Text (3)").GetComponent<TextMesh> ().text = text;
		transform.Find ("Text/Text (4)").GetComponent<TextMesh> ().text = text;

		transform.Find ("Text").GetComponent<TextMesh> ().color = c;
	}

}

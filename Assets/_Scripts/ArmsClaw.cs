﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsClaw : Enemy {

	public enum clawModes
	{
		followShip,
		spreadShot,
		aimShot
	};

	public clawModes clawMode;

	public GameObject masterShip;
	[HideInInspector]
	public Vector3 shipOffset; // The claw's resting position relative to the master ship

	[HideInInspector]
	public Vector3[] points; // Stores the p0 and p1 for interpolation
	[HideInInspector]
	public float timeStart; // Birth time for this Enemy
	//[HideInInspector]
	public float duration = 2; // Duration of movement

	public GameObject projectilePrefab;
	public Color projectileColor;
	public float projectileDamage = 1;

	public AudioClip projectileSound;

	//

	void Awake () {
		clawMode = clawModes.followShip;

		if (masterShip != null) {
			masterShip.GetComponent<ArmsShip> ().claws.Add(gameObject);
		}

		showDamageForFrames = 5;

		materials = Utils.GetAllMaterials (gameObject);
		originalColors = new Color[materials.Length];
		for (int i = 0; i < materials.Length; i++) {
			originalColors[i] = materials[i].color;
		}
	}

	// Use this for initialization
	void Start () {
		points = new Vector3[2];

		points [0] = transform.position;
		points [1] = transform.position;

		InitMovement ();

		InvokeRepeating("changeClawMode", 5f, 5f);

		InvokeRepeating("fireBarrage", 1f, fireRate);
	}
	
	// Update is called once per frame
	void Update () {
		Move();

		if (remainingDamageFrames > 0) {
			remainingDamageFrames--;
			if (remainingDamageFrames <= 0) {
				UnShowDamage ();
			}
		}
	}

	void InitMovement () {
		// Pick a new point to move to that is on screen
		Vector3 p1 = Vector3.zero;
		float esp = Main.S.enemySpawnPadding;
		Bounds cBounds = Utils.camBounds;
		p1.x = Random.Range (cBounds.min.x + esp, cBounds.max.x - esp);
		p1.y = Random.Range (20 + esp, cBounds.max.y - esp);
		//p1.y = Random.Range (cBounds.min.y + esp, cBounds.max.y - esp);

		//points [0] = points [1]; // Shift points[1] to points[0]
		points [0] = transform.position;
		points[1] = p1; // Add p1 as points[1]

		duration = (points[1] - points[0]).magnitude / 10f;

		// Reset the time
		timeStart = Time.time;
	}

	public override void Move () {
		switch (clawMode) {
		case clawModes.followShip:
			if (masterShip != null)
				transform.position = Vector3.Lerp (transform.position, masterShip.transform.position + shipOffset, 0.1f);

			break;
		case clawModes.spreadShot:
			float u = (Time.time - timeStart) / duration;
			if (u >= 1) { // if u >= 1...
				InitMovement(); /// then initialize movement to a new point
				u = 0;
			}

			u = 1 - Mathf.Pow (1 - u, 2); // Apply Ease Out easing to u

			pos = (1 - u) * points [0] + u * points [1]; // Simple linear interpolation

			//Vector3.Lerp (transform.position, points[1], 0.2f);

			break;
		case clawModes.aimShot:

			break;
		}

	}

	void OnCollisionEnter (Collision col) {
		GameObject other = col.gameObject;
		Debug.Log ("Enemy hit by " + other.gameObject.name);
		switch (other.tag) {
		case "Hero":
			other.GetComponent<Hero>().changeShieldLevel( -contactDamage );
			break;

		case "ProjectileHero":
			Projectile p = other.GetComponent<Projectile> ();
			// Enemies don't take damage unless they're on the screen
			// This stops the player from shooting them before they are visible
			bounds.center = transform.position + boundsCenterOffset;
			//if (bounds.extents == Vector3.zero || Utils.ScreenBoundsCheck (bounds, BoundsTest.offScreen) != Vector3.zero) {
			//	Destroy (other);
			//	break;
			//}
			// Hurt this enemy
			ShowDamage ();
			Main.S.makeSound (hitSound);

			// Get the damage amount from the Projectile.type and Main.W_DEFS
			health -= Main.W_DEFS [p.type].damageOnHit;

			if (health <= 0) {
				Explode ();

				if (masterShip.GetComponent<ArmsShip> ().claws.Count == 3)
					DialogBox.S.setDialog (masterShip.GetComponent<ArmsShip> ().loseClaw1Dialog);
				else if (masterShip.GetComponent<ArmsShip> ().claws.Count == 2)
					DialogBox.S.setDialog (masterShip.GetComponent<ArmsShip> ().loseClaw2Dialog);
				else if (masterShip.GetComponent<ArmsShip> ().claws.Count == 1)
					DialogBox.S.setDialog (masterShip.GetComponent<ArmsShip> ().loseClaw3Dialog);
				else if (masterShip.GetComponent<ArmsShip> ().claws.Count == 0)
					DialogBox.S.setDialog (masterShip.GetComponent<ArmsShip> ().loseClaw4Dialog);
			}
			Destroy (other);
			break;
		}
	}

	public void fireBarrage() {
		if (Main.S.gameMode == Main.gameModes.playing) {
			fireProjectile ();
			Invoke ("fireProjectile", 0.2f);
			Invoke ("fireProjectile", 0.4f);
		}
	}

	public void fireProjectile() {
		GameObject p = Instantiate (projectilePrefab) as GameObject;

		p.transform.position = transform.position;
		p.transform.parent = Weapon.PROJECTILE_ANCHOR;

		p.GetComponent<Projectile> ().hitDamage = projectileDamage;

		p.GetComponent<Renderer>().material.color = projectileColor;
		p.GetComponent<Rigidbody> ().velocity = Vector3.down * 40f;

		Main.S.makeSound (projectileSound);
	}


	public void changeClawMode() {
		if (clawMode == clawModes.followShip && Main.S.gameMode == Main.gameModes.playing) {
			clawMode = clawModes.spreadShot;
			InitMovement ();
		}
		else if (clawMode == clawModes.spreadShot)
			clawMode = clawModes.followShip;
	}

	public override void Explode() {
		if (isDestroyed == false) {
			isDestroyed = true;
			// Tell the Main singleton that this ship has been destroyed
			Main.S.EnemyDestroyed (this);
			// Destroy this Enemy
			Destroy (this.gameObject);

			Main.S.makeSound (deathSound);

			if (explosionPrefab != null)
				Instantiate (explosionPrefab, transform.position, Quaternion.identity);

			masterShip.GetComponent<ArmsShip> ().claws.Remove (gameObject);
		}
	}
}

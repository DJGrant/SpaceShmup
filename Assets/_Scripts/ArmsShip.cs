﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsShip : MonoBehaviour {

	public float speed = 10f;
	public float fireRate = 0.3f;
	public float health = 100;

	public float contactDamage;

	[HideInInspector]
	public List<GameObject> claws;
	public GameObject clawPrefab;
	public int clawCount = 4;

	[HideInInspector]
	public int showDamageForFrames;
	[HideInInspector]
	public Color[] originalColors;
	[HideInInspector]
	public Material[] materials;
	[HideInInspector]
	public int remainingDamageFrames = 0;

	//public float powerUpDropChance = 1f; // Chance to drop a PowerUp

	public GameObject explosionPrefab;

	[HideInInspector]
	public Vector3[] points; // Stores the p0 and p1 for interpolation
	[HideInInspector]
	public float timeStart; // Birth time for this Enemy_4
	//[HideInInspector]
	public float duration = 2; // Duration of movement

	[HideInInspector]
	public Bounds bounds;
	[HideInInspector]
	public Vector3 boundsCenterOffset;

	public GameObject escapePodPrefab;
	public Color podWindowColor;
	public Color podWallColor;

	public AudioClip hitSound;
	public AudioClip explosionSound;

	public DialogInstance[] loseClaw1Dialog;
	public DialogInstance[] loseClaw2Dialog;
	public DialogInstance[] loseClaw3Dialog;
	public DialogInstance[] loseClaw4Dialog;

	public DialogInstance[] WreckedDialog;

	void Awake () {
		showDamageForFrames = 5;

		materials = Utils.GetAllMaterials (gameObject);
		originalColors = new Color[materials.Length];
		for (int i = 0; i < materials.Length; i++) {
			originalColors[i] = materials[i].color;
		}

		InvokeRepeating ("CheckOffscreen", 0f, 2f);
	}

	// Use this for initialization
	void Start () {
		for (int i = 0; i < clawCount; i++) { // Make sure we have at least the specified number of claws
			GameObject c = Instantiate (clawPrefab, transform.position, Quaternion.identity);
			c.GetComponent<ArmsClaw>().masterShip = gameObject;
			claws.Add (c);
		}


		float radius = 10f;

		float angle = 0f;
		angle -= (270f);

		for (int i = 0; i < claws.Count; i++) { // Set the position offsets of all the claws
			float x = -(Mathf.Sin (Mathf.Deg2Rad * angle) * radius);
			float y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;

			claws[i].GetComponent<ArmsClaw>().shipOffset = new Vector3 (x, y, 0f);

			angle += (180f / (claws.Count - 1));
		}

		points = new Vector3[2];
		// There is already an initial position chosen by Main.SpawnEnemy90
		// so add it to the points as the initial p0 and p1
		points [0] = pos;
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = pos; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		InitMovement ();
	}

	// Update is called once per frame
	void Update () {
		Move();

		if (remainingDamageFrames > 0) {
			remainingDamageFrames--;
			if (remainingDamageFrames <= 0) {
				UnShowDamage();
			}
		}
	}

	void InitMovement () {
		// Pick a new point to move to that is on screen
		Vector3 p1 = Vector3.zero;
		//float esp = Main.S.enemySpawnPadding;
		float esp = 8;
		Bounds cBounds = Utils.camBounds;
		p1.x = Random.Range (cBounds.min.x + esp, cBounds.max.x - esp);
		p1.y = Random.Range (20 + esp, cBounds.max.y - esp);
		//p1.y = Random.Range (cBounds.min.y + esp, cBounds.max.y - esp);

		points [0] = pos; // Shift points[1] to points[0]
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = p1; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		duration = (points[1] - points[0]).magnitude / 10f;

		// Reset the time
		timeStart = Time.time;
	}

	public void Move () {
		// Linear interpolation

		float u = (Time.time - timeStart) / duration;
		if (u >= 1) { // if u >= 1...
			InitMovement(); /// then initialize movement to a new point
			u = 0;
		}

		u = 1 - Mathf.Pow (1 - u, 2); // Apply Ease Out easing to u

		pos = (1 - u) * points [0] + u * points [1]; // Simple linear interpolation
	}

	public Vector3 pos {
		get {
			return (this.transform.position);
		}
		set {
			this.transform.position = value;
		}
	}

	void CheckOffscreen() {
		// If bounds are still their default value...
		if (bounds.size == Vector3.zero) {
			// then set them
			bounds = Utils.CombineBoundsOfChildren(this.gameObject);
			// Also find the difference between bounds.center and transform.position
			boundsCenterOffset = bounds.center - transform.position;
		}

		// Every time, update the bounds to the current position
		bounds.center = transform.position + boundsCenterOffset;
		//Check to see whether the bounds are completely offscreen
		Vector3 off = Utils.ScreenBoundsCheck(bounds, BoundsTest.offScreen);
		if (off != Vector3.zero) {
			// if this enemy has gone off the bottom edge of the screen
			if (off.y < 0) {
				// then destroy it
				//Destroy (this.gameObject);
				// Actually, it's a boss, so don't
			}
		}
	}

	void OnCollisionEnter (Collision col) {
		GameObject other = col.gameObject;
		Debug.Log ("Enemy hit by " + other.gameObject.name);
		switch (other.tag) {
		case "Hero":
			other.GetComponent<Hero>().changeShieldLevel( -contactDamage );
			break;

		case "ProjectileHero":
			Projectile p = other.GetComponent<Projectile> ();
			// Enemies don't take damage unless they're on the screen
			// This stops the player from shooting them before they are visible
			bounds.center = transform.position + boundsCenterOffset;
			if (bounds.extents == Vector3.zero || Utils.ScreenBoundsCheck (bounds, BoundsTest.offScreen) != Vector3.zero) {
				Destroy (other);
				break;
			}
			// Hurt this enemy
			ShowDamage();
			Main.S.makeSound (hitSound);
			// Get the damage amount from the Projectile.type and Main.W_DEFS
			health -= Main.W_DEFS [p.type].damageOnHit;
			if (health <= 0) {
				Explode ();
			}
			Destroy (other);
			break;
		}
	}

	void ShowDamage () {
		foreach (Material m in materials) {
			m.color = Color.red;
		}
		remainingDamageFrames = showDamageForFrames;
	}
	void UnShowDamage () {
		for (int i = 0; i < materials.Length; i++) {
			materials [i].color = originalColors [i];
		}
	}

	public void Explode() {
		// Tell the Main singleton that this ship has been destroyed
		//Main.S.EnemyDestroyed (this);

		DialogBox.S.setDialog (WreckedDialog);

		// Destroy all claws
		GameObject[] clawDestroys = new GameObject[claws.Count];
		for (int i = 0; i < claws.Count; i++)
			clawDestroys[i] = claws[i];
		foreach (GameObject c in clawDestroys)
			c.GetComponent<ArmsClaw> ().Explode ();

		// Destroy this Enemy
		Destroy (this.gameObject);

		Main.S.makeSound (explosionSound);

		if (explosionPrefab != null)
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);

		if (escapePodPrefab != null) {
			GameObject pod = Instantiate (escapePodPrefab, transform.position, Quaternion.identity) as GameObject;
			pod.GetComponent<EscapePod> ().windowColor = podWindowColor;
			pod.GetComponent<EscapePod> ().wallColor = podWallColor;
		}
	}

}

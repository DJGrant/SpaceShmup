﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

	Renderer rend;

	public float scrollSpeed;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		float offset = Time.time * scrollSpeed * 0.01f;

		rend.material.SetTextureOffset ("_MainTex", new Vector2(0f, offset));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum DialogCharacter {
	Admiral,
	Arms,
	Tendrils,
	Spiny,
	Photrox
}

[System.Serializable]
public class DialogCharacterDef {
	public DialogCharacter name;
	public Sprite[] portraits;
	public Color boxColor;
	public AudioClip talkSound;
}

[System.Serializable]
public class DialogInstance {
	public DialogCharacter character;
	public int portraitNumber;
	[TextArea]
	public string dialog;
	public float holdTime; // Time the text stays on-screen
}

/*
[System.Serializable]
public class DialogBatch {
	public DialogInstance[] dialogs;
}
*/



public class DialogBox : MonoBehaviour {

	static public DialogBox S;

	public DialogCharacterDef[] characterDefinitions;
	static public Dictionary<DialogCharacter, DialogCharacterDef> CHAR_DEFS;

	[HideInInspector]
	public Image portraitArea;
	[HideInInspector]
	public Image textBox;
	[HideInInspector]
	public Text textArea;

	string dialog; // The intended message to be displayed
	float timer; // Time til the dialog dissapears
	int textIndex; // How many characters are displayed so far? (While writing the text)

	DialogInstance[] dialogList;
	int dialogIndex; // What message are we on now? (Entire message)

	AudioSource audioSource;

	void Awake () {
		S = this;

		CHAR_DEFS = new Dictionary<DialogCharacter, DialogCharacterDef> ();
		foreach (DialogCharacterDef def in characterDefinitions) {
			CHAR_DEFS [def.name] = def;
		}
	
		portraitArea = transform.Find ("PortraitBG/Portrait").gameObject.GetComponent<Image>();
		textBox = transform.Find ("Box").gameObject.GetComponent<Image>();
		textArea = transform.Find ("Box/Text").gameObject.GetComponent<Text>();

		audioSource = GetComponent<AudioSource> ();

		if (dialog == null)
			dialog = "";
		if (dialogList == null)
			dialogList = new DialogInstance[0];
	}
	
	// Update is called once per frame
	void Update () {
		if (textIndex <= dialog.Length) {
			textArea.text = dialog.Substring (0, textIndex) + "<color=#00000000>" + dialog.Substring (textIndex, dialog.Length-textIndex) + "</color>";
			textIndex ++;

			if (!audioSource.isPlaying)
				audioSource.Play ();
		} else {
			timer -= Time.deltaTime;
		}

		if (timer <= 0f) {
			dialogIndex++;

			if (dialogIndex < dialogList.Length) {
				setDialogInstance (dialogList [dialogIndex]);
			} else {
				gameObject.GetComponent<Animator>().SetBool ("IsOpen", false);

				// We want the end of the dialog to mean the end of the cutscene
				if (Main.S != null) {
					if (Main.S.gameMode == Main.gameModes.cutscene)
						Main.S.gameMode = Main.gameModes.playing;
				}
			}
		}
	}

	private void setDialogInstance (DialogCharacterDef c, int i, string str, float time) {

		if (i < c.portraits.Length)
			portraitArea.sprite = c.portraits [i];
		else
			portraitArea.sprite = c.portraits [0];

		textBox.color = c.boxColor;

		dialog = str;
		//textArea.text = "";
		textIndex = 0;

		timer = time;

		audioSource.clip = c.talkSound;
	}
	private void setDialogInstance (DialogInstance d) {
		setDialogInstance (getCharDef(d.character), d.portraitNumber, d.dialog, d.holdTime);
	}

	public DialogCharacterDef getCharDef(DialogCharacter d) { // Get a character definition
		if (CHAR_DEFS.ContainsKey(d)) {
			return CHAR_DEFS [d];
		}

		return (CHAR_DEFS[DialogCharacter.Admiral]);
	}

	public void setDialog (DialogInstance[] d) { // We pass in a list of DialogInstances
		if (d.Length > 0) {
			dialogList = d;
			dialogIndex = 0;

			setDialogInstance (dialogList [0]);

			gameObject.GetComponent<Animator> ().SetBool ("IsOpen", true);
		}
	}
	public void setDialog (DialogInstance d) { // Overload for if we just add one DialogInstance
		dialogList = new DialogInstance[1];
		dialogList[0] = d;
		dialogIndex = 0;

		setDialogInstance (dialogList [0]);
	}
		
}

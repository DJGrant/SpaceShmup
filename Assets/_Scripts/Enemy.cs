﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public float speed = 10f;
	public float fireRate = 0.3f;
	public float health = 10;
	public int score = 100;

	public float contactDamage;

	[HideInInspector]
	public int showDamageForFrames;
	[HideInInspector]
	public Color[] originalColors;
	[HideInInspector]
	public Material[] materials;
	[HideInInspector]
	public int remainingDamageFrames = 0;

	public float powerUpDropChance = 1f; // Chance to drop a PowerUp

	public GameObject explosionPrefab;

	[HideInInspector]
	public Bounds bounds;
	[HideInInspector]
	public Vector3 boundsCenterOffset;

	public AudioClip hitSound;
	public AudioClip deathSound;

	protected bool isDestroyed = false;

	void Awake () {
		showDamageForFrames = 5;

		materials = Utils.GetAllMaterials (gameObject);
		originalColors = new Color[materials.Length];
		for (int i = 0; i < materials.Length; i++) {
			originalColors[i] = materials[i].color;
		}

		InvokeRepeating ("CheckOffscreen", 0f, 2f);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Move();

		if (remainingDamageFrames > 0) {
			remainingDamageFrames--;
			if (remainingDamageFrames <= 0) {
				UnShowDamage();
			}
		}
	}

	public virtual void Move() {
		Vector3 tempPos = pos;
		tempPos.y -= speed * Time.deltaTime;
		pos = tempPos;
	}

	public Vector3 pos {
		get {
			return (this.transform.position);
		}
		set {
			this.transform.position = value;
		}
	}

	void CheckOffscreen() {
		// If bounds are still their default value...
		if (bounds.size == Vector3.zero) {
			// then set them
			bounds = Utils.CombineBoundsOfChildren(this.gameObject);
			// Also find the difference between bounds.center and transform.position
			boundsCenterOffset = bounds.center - transform.position;
		}

		// Every time, update the bounds to the current position
		bounds.center = transform.position + boundsCenterOffset;
		//Check to see whether the bounds are completely offscreen
		Vector3 off = Utils.ScreenBoundsCheck(bounds, BoundsTest.offScreen);
		if (off != Vector3.zero) {
			// if this enemy has gone off the bottom edge of the screen
			if (off.y < 0) {
				// then destroy it
				Destroy (this.gameObject);
			}
		}
	}

	void OnCollisionEnter (Collision col) {
		GameObject other = col.gameObject;
		Debug.Log ("Enemy hit by " + other.gameObject.name);
		switch (other.tag) {
		case "Hero":
			other.GetComponent<Hero>().changeShieldLevel( -contactDamage );
			crashSplode ();
			break;

		case "ProjectileHero":
			Projectile p = other.GetComponent<Projectile> ();
			// Enemies don't take damage unless they're on the screen
			// This stops the player from shooting them before they are visible
			bounds.center = transform.position + boundsCenterOffset;
			if (bounds.extents == Vector3.zero || Utils.ScreenBoundsCheck (bounds, BoundsTest.offScreen) != Vector3.zero) {
				Destroy (other);
				break;
			}
			// Hurt this enemy
			ShowDamage ();
			Main.S.makeSound (hitSound);
			// Get the damage amount from the Projectile.type and Main.W_DEFS
			health -= Main.W_DEFS [p.type].damageOnHit;
			if (health <= 0) {
				Explode ();
			}
			Destroy (other);
			break;
		}
	}

	protected void ShowDamage () {
		foreach (Material m in materials) {
			m.color = Color.red;
		}
		remainingDamageFrames = showDamageForFrames;
	}
	protected void UnShowDamage () {
		for (int i = 0; i < materials.Length; i++) {
			materials [i].color = originalColors [i];
		}
	}

	public virtual void Explode() {
		if (isDestroyed == false) {
			isDestroyed = true;
			// Tell the Main singleton that this ship has been destroyed
			Main.S.EnemyDestroyed (this);
			// Destroy this Enemy
			Destroy (this.gameObject);

			if (explosionPrefab != null)
				Instantiate (explosionPrefab, transform.position, Quaternion.identity);

			Main.S.makeSound (deathSound);
		}
	}

	public virtual void crashSplode() {
		// Destroy this Enemy
		Destroy (this.gameObject);

		if (explosionPrefab != null)
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);

		Main.S.makeSound (deathSound);
	}
}

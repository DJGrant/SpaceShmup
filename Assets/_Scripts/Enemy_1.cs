﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy_1 ectends the enemy class
public class Enemy_1 : Enemy {

	// # of seconds for a full sine wave
	public float waveFrequency = 2;
	// sine wave width in meters (units)
	public float waveWidth = 10;
	public float waveRotY = 45;

	private float x0 = -12345; // The initial x value of pos
	private float birthTime;

	// Use this for initialization
	void Start () {
		x0 = pos.x;

		birthTime = Time.time;
	}
	
	// Override the move function on the Enemy
	public override void Move () {
		// Because pos is a property, you can't directly set pos.x
		// so get the pos as an editable Vector3
		Vector3 tempPos = pos;
		// theta adjusts based on time
		float age = Time.time - birthTime;
		float theta = Mathf.PI * 2 * age / waveFrequency;
		float sin = Mathf.Sin (theta);
		tempPos.x = x0 + waveWidth * sin;
		pos = tempPos;

		// rotate a bit about y
		Vector3 rot = new Vector3 (0, sin * waveRotY, 0);
		this.transform.rotation = Quaternion.Euler (rot);
		// base.Move() still handles the movement down in y
		base.Move();
	}
}

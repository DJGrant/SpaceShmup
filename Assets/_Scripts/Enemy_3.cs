﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_3 : Enemy {
	// Enemy_3 will Move following a bezier curve, which is a linear
	// interpolation between more than two points.
	public Vector3[] points;
	public float birthTime;
	public float lifeTime = 10;

	public GameObject projectilePrefab;
	public Color projectileColor;
	public float projectileDamage = 1;

	public AudioClip projectileSound;

	// Have I ever mentioned that the reason Start() works so well
	// here is because the original Enemy class never uses it?

	// Use this for initialization
	void Start () {
		points = new Vector3[3];

		// The start position has already been set by Main.SpawnEnemy()
		points[0] = pos;

		// Set xMin and xMax the same way that Main.SpawnEnemy90 does
		float xMin = Utils.camBounds.min.x + Main.S.enemySpawnPadding;
		float xMax = Utils.camBounds.max.x - Main.S.enemySpawnPadding;

		Vector3 v;
		// Pick a random middle position in the bottom half of the screen
		v = Vector3.zero;
		v.x = Random.Range (xMin, xMax);
		v.y = Random.Range (Utils.camBounds.min.y, 0);
		points [2] = v;

		// Set the birthTime to the current time
		birthTime = Time.time;

		InvokeRepeating ("fireProjectiles", 2f, 2f);
	}
	
	public override void Move() {
		// Bezier curves work based on a u value between 0 and 1
		float u = (Time.time - birthTime) / lifeTime;
		if (u > 1) {
			// This Enemy_3 has finished its life
			Destroy (this.gameObject);
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			return;
		}

		// Interpolate the three Bezier curve points
		Vector3 p01, p12;
		p01 = (1 - u) * points [0] + u * points [1];
		p12 = (1 - u) * points [1] + u * points [2];
		pos = (1 - u) * p01 + u * p12;
	}

	public void fireProjectiles() {
		GameObject p1 = Instantiate (projectilePrefab) as GameObject;
		GameObject p2 = Instantiate (projectilePrefab) as GameObject;

		p1.transform.position = transform.position + new Vector3(1f, 0f, 0f);
		p1.transform.parent = Weapon.PROJECTILE_ANCHOR;
		p2.transform.position = transform.position + new Vector3(-1f, 0f, 0f);
		p2.transform.parent = Weapon.PROJECTILE_ANCHOR;

		p1.GetComponent<Projectile> ().hitDamage = projectileDamage;
		p2.GetComponent<Projectile> ().hitDamage = projectileDamage;

		p1.GetComponent<Renderer>().material.color = projectileColor;
		p1.GetComponent<Rigidbody> ().velocity = Vector3.down * 30f;
		p2.GetComponent<Renderer>().material.color = projectileColor;
		p2.GetComponent<Rigidbody> ().velocity = Vector3.down * 30f;

		Main.S.makeSound (projectileSound);
	}
}

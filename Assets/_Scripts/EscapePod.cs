﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapePod : MonoBehaviour {

	public Color windowColor;
	public Color wallColor;

	public GameObject captureWord;
	public AudioClip captureSound;

	bool pickedUp = false;

	Vector3 rotPerSecond;

	// Use this for initialization
	void Start () {
		rotPerSecond = new Vector3(10f,10f,10f);

		transform.Find ("Cube/Sphere").GetComponent<Renderer>().material.color = windowColor;

		transform.Find ("Cube").GetComponent<Renderer>().material.color = wallColor;
		//transform.Find ("Cube/Cube (1)").GetComponent<Renderer>().material.color = wallColor;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Euler(rotPerSecond* Time.time);

		if (pickedUp) {
			if (transform.localScale.x > 0.02f && transform.localScale.y > 0.02f && transform.localScale.z > 0.02f)
				transform.localScale -= new Vector3 (0.02f, 0.02f, 0.02f);
			else
				Destroy(gameObject);
		}
	}

	void OnCollisionEnter (Collision col) {
		if (col.gameObject.tag == "Hero" && pickedUp == false) {
			Instantiate (captureWord, col.gameObject.transform.position, Quaternion.identity);
			pickedUp = true;

			Main.S.makeSound (captureSound);

			Main.S.Invoke ("loadNextScene", 4f);
		}
	}



}

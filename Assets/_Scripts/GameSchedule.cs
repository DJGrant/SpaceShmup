﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GamePhase {
	public float phaseDuration; // Duration of the phase in seconds

	public GameObject[] prefabEnemies;
	public float enemySpawnPerSecond = 0.5f; // Enemies per second
	public float enemySpawnPadding = 1.5f; // Padding for position
	public WeaponType[] powerUpFrequency;

	public DialogInstance[] phaseStartDialog;
}

public class GameSchedule : MonoBehaviour {

	static public GameSchedule S;

	public string nextScene;

	public AudioClip stageMusic;

	public GamePhase[] gamePhases;
	public GamePhase bossPhase;
	[HideInInspector]
	public int currentPhaseIndex = 0;
	[HideInInspector]
	public float currentPhaseStartTime;

	//public DialogInstance[] BossDialog;

	bool BossEntered = false;
	bool BossExited = false;
	public GameObject BossPrefab;
	GameObject Boss;
	public AudioClip BossMusic;

	void Awake () {
		S = this;
	}

	// Use this for initialization
	void Start () {
		Main.S.nextScene = nextScene;

		currentPhaseStartTime = Time.time;
		if (gamePhases.Length > 0) {
			SetPhase (gamePhases [0]);
		}

		Main.S.changeMusic (stageMusic);
	}
	
	// Update is called once per frame
	void Update () {
		if (currentPhaseIndex < gamePhases.Length) {
			if (Time.time - currentPhaseStartTime > gamePhases [currentPhaseIndex].phaseDuration)
				NextPhase ();
		} else if (GameObject.FindGameObjectsWithTag ("Enemy").Length == 0 && BossPrefab != null && BossEntered == false) {
			Boss = Instantiate (BossPrefab, new Vector3 (0f, 64f, 0f), Quaternion.identity);
			BossEntered = true;
			SetPhase (bossPhase);

			if (bossPhase.phaseStartDialog != null && bossPhase.phaseStartDialog.Length > 0) {
				Main.S.gameMode = Main.gameModes.cutscene;
				//DialogBox.S.setDialog (GameSchedule.S.BossDialog);
			}

			Main.S.stopMusic ();
			SetBossMusic ();

		} else if (BossEntered == true && BossExited == false && Boss == null) {
			BossExited = true;
			SetEmptyPhase (); // We don't want enemies spawing after the boss has been defeated
			Main.S.stopMusic();
		}
	}

	void NextPhase() {
		currentPhaseIndex++;
		if (currentPhaseIndex < gamePhases.Length) {
			SetPhase (gamePhases[currentPhaseIndex]);
		} else {
			//SetPhase (bossPhase);
			SetNoEnemies(); // I made this function not reset the PowerUp list because we
			// still might want to collect PowerUps from enemies right before the boss enters
		}
	}

	void SetPhase(GamePhase g) {
		Main.S.prefabEnemies = g.prefabEnemies;
		Main.S.enemySpawnPerSecond = g.enemySpawnPerSecond;
		Main.S.enemySpawnPadding = g.enemySpawnPadding;
		Main.S.powerUpFrequency = g.powerUpFrequency;

		DialogBox.S.setDialog (g.phaseStartDialog);

		currentPhaseStartTime = Time.time;
	}

	void SetEmptyPhase() {
		Main.S.prefabEnemies = new GameObject[0];
		Main.S.enemySpawnPerSecond = 0f;
		Main.S.enemySpawnPadding = 1.5f;
		Main.S.powerUpFrequency = new WeaponType[0];

		currentPhaseStartTime = Time.time;
	}
	void SetNoEnemies() {
		Main.S.prefabEnemies = new GameObject[0];
		Main.S.enemySpawnPerSecond = 0f;
		Main.S.enemySpawnPadding = 1.5f;

		currentPhaseStartTime = Time.time;
	}

	void SetBossMusic() {
		if (Main.S.gameMode == Main.gameModes.playing)
			Main.S.changeMusic (BossMusic);
		else
			Invoke ("SetBossMusic", 1f);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour {

	static public Hero S; // Singleton, eh? I guess I'm finally learning about singletons.

	// Ship movement stuff
	public float speed = 30;
	public float rollMult = -45;
	public float pitchMult = 30;

	// Stats and stuff
	public float maxShieldLevel = 10;
	[SerializeField]
	private float _shieldLevel = 1;
	float displayedShieldLevel = 0f;

	public float gameRestartDelay = 2f;

	// Weapon fields
	public Weapon[] weapons;
	[HideInInspector]
	public int weaponCount = 1;
	[HideInInspector]
	public WeaponType currentWeaponType;

	public GameObject explosionPrefab;

	public AudioClip weaponChangeSound;
	public AudioClip weaponAddSound;
	public AudioClip HPPlusSound;
	public AudioClip projectileSound;
	public AudioClip damageSound;
	public AudioClip deathSound;

	GameObject shipGlowyBits;


	Rigidbody rb;
	GameObject ship;
	LineRenderer hpRing;
	[HideInInspector]
	public Bounds bounds;

	// Declare a new delegate type WeaponFireDelegate
	public delegate void WeaponFireDelegate();
	// Create a WeaponFireDelegate field named fireDelegate
	public WeaponFireDelegate fireDelegate;

	[HideInInspector]
	public float lastShot = 0;

	float deltaTime;

	void Awake() {
		S = this; // So this is how you set singletons. Kind of like I did with the GameManagers;

		bounds = Utils.CombineBoundsOfChildren(this.gameObject);

		fireDelegate += playProjectileSound;
	}

	// Use this for initialization
	void Start () {
		// Reset the weapons to start _Hero with one blaster
		ClearWeapons();
		currentWeaponType = WeaponType.blaster;
		setWeapons ();

		//shieldLevel = _shieldLevel;
		shieldLevel = maxShieldLevel;

		rb = GetComponent<Rigidbody>();
		ship = transform.GetChild (0).gameObject;
		hpRing = GetComponent<LineRenderer> ();

		if (transform.Find ("Ship/ShipGlowyBits") != null) {
			shipGlowyBits = gameObject.transform.Find ("Ship/ShipGlowyBits").gameObject;
			shipGlowyBits.GetComponent<Renderer> ().material.color = Main.GetWeaponDefinition (currentWeaponType).color;
		}
	}
	
	// Update is called once per frame
	public void FixedUpdate ()
	{
		float delta = Time.deltaTime;
		while (delta > Time.fixedDeltaTime) {
			deltaTime = Time.fixedDeltaTime;
			SingleUpdate ();
			delta -= Time.fixedDeltaTime;
		}

		if (delta > 0f) {
			deltaTime = delta;
			SingleUpdate ();
		}

		if (Input.GetAxis ("Fire1") == 1 && fireDelegate != null && Main.S.gameMode == Main.gameModes.playing) {
			fireDelegate ();

			Debug.Log ("Ker-Pow!");
		}
	}

	void SingleUpdate () {
		rb.velocity = Vector3.zero; // So we don't get any weirdness when we push or get pushed by objects.

		if (Main.S.gameMode == Main.gameModes.playing) {
			float xAxis = Input.GetAxis ("Horizontal");
			float yAxis = Input.GetAxis ("Vertical");


			Vector3 pos = transform.position;
			pos.x += xAxis * speed * deltaTime;
			pos.y += yAxis * speed * deltaTime;

			//transform.position = pos;
			rb.MovePosition (pos);
		

			bounds.center = transform.position;

			//Debug.Log (Utils.camBounds);


			// Keep the ship constrained to the screen bounds
			Vector3 off = Utils.ScreenBoundsCheck (bounds, BoundsTest.onScreen);
			if (off != Vector3.zero) {
				pos -= off;
				//transform.position = pos;
				rb.MovePosition (pos);
			}



			ship.transform.rotation = Quaternion.Euler (yAxis * pitchMult, xAxis * rollMult, 0f);
		} else {
			rb.MovePosition (Vector3.Lerp(transform.position, new Vector3(0f,-20f,0f), 0.05f));
			ship.transform.rotation = Quaternion.Euler (0f, 0f, 0f);
		}

		drawHealthRing (); // Draw the Shield/Health Ring
	}

	void drawHealthRing() {
		int segments = 42;

		if (displayedShieldLevel != shieldLevel) {
			displayedShieldLevel = Mathf.Lerp (displayedShieldLevel, shieldLevel, 0.1f);
			if (Mathf.Abs (displayedShieldLevel - shieldLevel) < 0.5f)
				displayedShieldLevel = shieldLevel;
		}
		int activeSegments = (int)((displayedShieldLevel * (float)segments) / maxShieldLevel);

		if (activeSegments == segments)
			hpRing.loop = true;
		else
			hpRing.loop = false;

		Color HPColor = Color.Lerp(Color.red, Color.green, (displayedShieldLevel / maxShieldLevel));
		HPColor.a = 0.5f;
		hpRing.startColor = HPColor;
		hpRing.endColor = HPColor;

		float radius = 3.65f;

		hpRing.positionCount = (activeSegments + 1);

		float angle = 0f;
		for (int i = 0; i < (activeSegments + 1); i++) {
			float x = -(Mathf.Sin (Mathf.Deg2Rad * angle) * radius);
			float y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;

			hpRing.SetPosition (i, new Vector3(x, y, -1f));

			angle += (360f / segments);
		}
	}

	public float shieldLevel {
		get {
			return (_shieldLevel);
		}
		set {
			_shieldLevel = Mathf.Min (value, maxShieldLevel);
			if (value < 0) {
				Explode ();
			}
		}
	}
	public void changeShieldLevel(float x) {
		if (Main.S.gameMode == Main.gameModes.playing) {
			shieldLevel += x;
			if (x < 0) {
				GetComponent<Animator> ().SetTrigger ("IsDamaged");
				Main.S.makeSound (damageSound);
			}
		}
	}


	// The last object we collided with or triggered
	[HideInInspector]
	public GameObject lastColObj = null;

	void OnCollisionEnter (Collision other) {
		GameObject obj = Utils.FindTaggedParent (other.gameObject);
		// If there is a parent with a tag
		if (obj != null) {
			// If we touched an enemy, decrease health
			if (obj.tag == "Enemy") {
				//shieldLevel--;
				//Destroy (obj); // Destroy the enemy. I MIGHT CHANGE THIS!
			} else if (obj.tag == "ProjectileEnemy") {
				Projectile p = other.gameObject.GetComponent<Projectile> ();
				// Enemies don't take damage unless they're on the screen
				// This stops the player from shooting them before they are visible
				changeShieldLevel (-p.hitDamage);
				Destroy (other.gameObject);
			} else {
				print ("Collided: " + obj.name);
			}


		} else {
			print ("Collided: " + other.gameObject.name);
		}

		// Make sure it's not the same one as last time
		if (obj == lastColObj) {
			return;
		}
		lastColObj = obj;
	}
		
	void OnTriggerEnter (Collider other) {
		GameObject obj = Utils.FindTaggedParent (other.gameObject);
		// If there is a parent with a tag
		if (obj != null) {
			// If we touched an enemy, decrease health
			if (obj.tag == "Enemy") {
				//shieldLevel--;
				//Destroy (obj); // Destroy the enemy. I MIGHT CHANGE THIS!
			} else if (obj.tag == "PowerUp") {
				AbsorbPowerUp (obj);
			} else {
				print ("Triggered: " + obj.name);
			}
		} else {
			print ("Triggered: " + other.gameObject.name);
		}

		// Make sure it's not the same one as last time
		if (obj == lastColObj) {
			return;
		}
		lastColObj = obj;
	}

	void OnCollisionStay (Collision other) {
		GameObject obj = Utils.FindTaggedParent (other.gameObject);
		if (obj != null && obj.tag == "Terrain") {
			Debug.Log ("Squarsh: " + other.contacts[0].normal + ", " + other.contacts[0].point + ", " + Utils.camBounds.min.y);
			if (other.contacts [0].normal.y <= -0.9f && other.contacts [0].point.y <= Utils.camBounds.min.y + 5f)
				Explode ();
		}
	}

	public void AbsorbPowerUp (GameObject obj) {
		PowerUp pu = obj.GetComponent<PowerUp>();
		switch (pu.type) {
		case WeaponType.shield:
			shieldLevel += 2f;
			Main.S.makeSound (HPPlusSound);
			break;
		
		case WeaponType.extraGun:
			/*Weapon w = GetEmptyWeaponSlot (); // Find an available weapon
			if (w != null) {
				// set it to pu.type
				w.SetType (weapons [0].type);
			}*/
			if (weaponCount < weapons.Length) {
				weaponCount++;
				setWeapons ();

				Main.S.makeSound (weaponAddSound);
			}
			break;

		default: // if it's any Weapon PowerUp
			// Check the current weapon type
			/*
			if (pu.type == weapons [0].type) {
				// Increase number of weapons of this type
				Weapon w = GetEmptyWeaponSlot (); // Find an available weapon
				if (w != null) {
					// set it to pu.type
					w.SetType (pu.type);
				}

			} else {
				// if this is a different weapon
				ClearWeapons ();
				weapons [0].SetType (pu.type);
			}
			*/
			currentWeaponType = pu.type;

			foreach (Weapon weap in weapons) {
				if (weap.type != WeaponType.none) {
					weap.SetType (currentWeaponType);
				}
			}

			Main.S.makeSound (weaponChangeSound);

			if (shipGlowyBits != null) {
				shipGlowyBits.GetComponent<Renderer> ().material.color = Main.GetWeaponDefinition (currentWeaponType).color;
			}

			break;
		}
		pu.AbsorbedBy (this.gameObject);
	}

	Weapon GetEmptyWeaponSlot () {
		for (int i = 0; i < weapons.Length; i++) {
			if (weapons [i].type == WeaponType.none) {
				return (weapons [i]);
			}
		}
		return (null);
	}

	void setWeapons () {
		Debug.Log (weaponCount);

		if (weaponCount <= 0) {
			ClearWeapons ();
			weapons [0].SetType (currentWeaponType);
			weaponCount = 1;
		} else if (weaponCount == 1) {
			ClearWeapons ();
			weapons [0].SetType (currentWeaponType);
		} else if (weaponCount == 2) {
			ClearWeapons ();
			weapons [3].SetType (currentWeaponType);
			weapons [4].SetType (currentWeaponType);
		} else if (weaponCount == 3) {
			ClearWeapons ();
			weapons [0].SetType (currentWeaponType);
			weapons [1].SetType (currentWeaponType);
			weapons [2].SetType (currentWeaponType);
		} else {
			if (weaponCount % 2 == 0) {
				ClearWeapons ();
				for (int i=0; i < weaponCount && i < weapons.Length; i++) 
					weapons [i+1].SetType (currentWeaponType);
			} else {
				ClearWeapons ();
				for (int i=0; i < weaponCount && i < weapons.Length; i++) 
					weapons [i].SetType (currentWeaponType);
			}
		}
	}

	void ClearWeapons () {
		foreach (Weapon w in weapons) {
			w.SetType (WeaponType.none);
		}
	}

	void Explode () {
		_shieldLevel = 0;
		if (explosionPrefab != null) {
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);
		}

		Destroy (this.gameObject);

		Main.S.makeSound (deathSound);

		Main.S.stopMusic();
		Main.S.DelayedRestart ( gameRestartDelay);
	}

	void playProjectileSound () {
		if (Time.time - lastShot < Main.GetWeaponDefinition(currentWeaponType).delayBetweenShots) {
			return;
		}
		Main.S.makeSound (projectileSound);
		lastShot = Time.time;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {

	static public Main S;
	static public Dictionary<WeaponType, WeaponDefinition> W_DEFS;

	public enum gameModes {playing, cutscene, gameover};
	public gameModes gameMode = gameModes.playing;
	public string nextScene;

	public GameObject[] prefabEnemies;
	public float enemySpawnPerSecond = 0.5f; // Enemies per second
	public float enemySpawnPadding = 1.5f; // Padding for position
	public WeaponDefinition[] weaponDefinitions;
	public GameObject prefabPowerUp;
	public WeaponType[] powerUpFrequency = new WeaponType[] {
		WeaponType.blaster, WeaponType.blaster,
		WeaponType.spread,
		WeaponType.shield
	};

	public WeaponType[] activeWeaponTypes;
	[HideInInspector]
	public float enemySpawnRate; // Delay between Enemy spawns

	public GameObject soundInstancePrefab;

	void Awake () {
		S = this;
		// Set Utils.camBounds
		Utils.SetCameraBounds(this.GetComponent<Camera>());
		// 0.5 enemies per second = enemySpawnRate of 2
		enemySpawnRate = 1f/enemySpawnPerSecond;
		// Invoke call SpawnEnemy() once after a 2 second delay;
		Invoke ("SpawnEnemy", enemySpawnRate);

		// A generic Dictionary with WeaponType as the key
		W_DEFS = new Dictionary<WeaponType, WeaponDefinition>();
		foreach (WeaponDefinition def in weaponDefinitions) {
			W_DEFS [def.type] = def;
		}

		Cursor.visible = false;
	}

	static public WeaponDefinition GetWeaponDefinition (WeaponType wt) {
		// Check to see if the key already exists in the dictionary
		if (W_DEFS.ContainsKey (wt)) {
			return (W_DEFS[wt]);
		}
		// This will return a definition for WeaponType.none,
		// which means it failed to find a weapon definition
		return (new WeaponDefinition());
	}

	// Use this for initialization
	void Start () {
		activeWeaponTypes = new WeaponType[weaponDefinitions.Length];
		for (int i = 0; i < weaponDefinitions.Length; i++) {
			activeWeaponTypes [i] = weaponDefinitions [i].type;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy() {
		if (prefabEnemies.Length > 0 && Main.S.gameMode == Main.gameModes.playing) {
			// Pick a random Enemy prefab to instatiate
			int ndx = Random.Range (0, prefabEnemies.Length);
			GameObject obj = Instantiate (prefabEnemies [ndx]) as GameObject;
			// Position the Enemy above the screen with a random x position
			Vector3 pos = Vector3.zero;
			float xMin = Utils.camBounds.min.x + enemySpawnPadding;
			float xMax = Utils.camBounds.max.x - enemySpawnPadding;
			pos.x = Random.Range (xMin, xMax);
			pos.y = Utils.camBounds.max.y + enemySpawnPadding;
			obj.transform.position = pos;
		}
		// Call SpawnEnemy again in a couple of seconds
		Invoke ("SpawnEnemy", 1f/enemySpawnPerSecond);
	}

	public void EnemyDestroyed (Enemy e) {
		// Potentially generate a PowerUp
		if (Random.value <= e.powerUpDropChance && powerUpFrequency.Length > 0) {
			// There's stuff to know about this, but I ain't copying it all down
			int ndx = Random.Range (0,powerUpFrequency.Length);
			WeaponType puType = powerUpFrequency [ndx];

			if (puType != WeaponType.none && puType != Hero.S.currentWeaponType) {
				if (puType == WeaponType.extraGun && Hero.S.weaponCount >= Hero.S.weapons.Length) {
					return;
				}

				// Spawn a PowerUp
				GameObject obj = Instantiate (prefabPowerUp) as GameObject;
				PowerUp pu = obj.GetComponent<PowerUp> ();
				// Set it to the proper WeaponType
				pu.SetType (puType);

				// Set it to the position of the destroyed ship
				pu.transform.position = e.transform.position;
			}
		}
	}

	public void makeSound(AudioClip audio) {
		if (soundInstancePrefab != null) {
			GameObject s = Instantiate (soundInstancePrefab, transform.position, Quaternion.identity);
			s.GetComponent<SoundInstance>().sound = audio;
		}
	}

	public void changeMusic(AudioClip a) {
		if (a != null) {
			GetComponent<AudioSource> ().clip = a;
			GetComponent<AudioSource> ().Play();
		}
	}
	public void stopMusic() {
			GetComponent<AudioSource> ().Stop();
	}

	public void DelayedRestart (float delay) {
		// Invoke the Restart() method after a certain number of seconds
		Invoke("Restart", delay);
	}

	public void Restart() {
		DialogBox.S.transform.parent.gameObject.GetComponent<Animator> ().SetTrigger ("FadeOut");
		Invoke ("restartRaw", 1f);
	}

	public void loadNextScene() {
		DialogBox.S.transform.parent.gameObject.GetComponent<Animator> ().SetTrigger ("FadeOut");
		Invoke ("nextSceneRaw", 1f);
	}


	void restartRaw() {
		// Reload the scene to restart the game
		SceneManager.LoadScene (SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
	void nextSceneRaw() {
		Debug.Log ("Next Scene! (" + nextScene + ")");
		if (nextScene != "")
			SceneManager.LoadScene (nextScene, LoadSceneMode.Single);
		else
			Application.Quit ();
	}
}

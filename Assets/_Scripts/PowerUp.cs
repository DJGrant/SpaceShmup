﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {
	// We're about to get weird but effective with our use of Vector2s.
	// x holds a min value and y holds a max value for a Random.Range that will be called later.
	public Vector2 rotMinMax = new Vector2(15,90);
	public Vector2 driftMinMax = new Vector2(0.25f, 2);
	public float lifeTime = 6f; // Seconds the PowerUp exists for
	public float fadeTime = 4f; // Seconds it will then fade

	public WeaponType type; // The type of PowerUp
	public GameObject cube; // Reference to the cube child
	public TextMesh letter; // Reference to the TextMesh
	public GameObject namePrefab;
	public Vector3 rotPerSecond; // Euler rotation speed
	public float birthTime;

	void Awake () {
		// Find the Cube reference
		cube = transform.Find("Cube").gameObject;
		// Find the TextMesh
		letter = GetComponent<TextMesh>();

		// Set a random velocity
		Vector3 vel = Random.onUnitSphere;
		vel.z = 0; // Flatten the vel to the XY plane
		vel.Normalize(); // Make the length of the vel 1
		vel *= Random.Range (driftMinMax.x, driftMinMax.y);
		// Above sets the velocity length to something between the x and y values of driftMinMax
		GetComponent<Rigidbody>().velocity = vel;

		// Zero out the rotation of this GameObject to [0,0,0]
		transform.rotation = Quaternion.identity;
		// Quaternion.identity is equal to no rotation, but I already knew that, so why'd I copy it down?

		// Set up the rotPerSecond for the Cube child using rotMinMax x and y
		rotPerSecond = new Vector3 (Random.Range (rotMinMax.x, rotMinMax.y),
			Random.Range (rotMinMax.x, rotMinMax.y),
			Random.Range (rotMinMax.x, rotMinMax.y));

		// CheckOffscreen every 2 seconds
		InvokeRepeating("CheckOffscreen", 2f, 2f);

		birthTime = Time.time;
	}

	// Use this for initialization
	void Start () {
		SetType (type);
	}
	
	// Update is called once per frame
	void Update () {
		// Manually rotate the Cube
		// Multiplying it by Time.time makes it time-based
		cube.transform.rotation = Quaternion.Euler(rotPerSecond* Time.time);

		//
		transform.position += Vector3.down * 0.1f;
		//

		// Fade the PowerUp over time
		// Given default values, a PowerUp will exist for 10 seconds and then fade out over 4
		float u = (Time.time - (birthTime + lifeTime)) / fadeTime;
		// For lifeTime seconds, u will be <= 0. Then it will transition to 1 over fadeTime seconds
		// If u > 1, destroy this PowerUp
		if (u >= 1) {
			Destroy (this.gameObject);
			return;
		}
		// Use u to determine the alpha value of the Cube and Letter
		if (u > 0) {
			Color c = cube.GetComponent<Renderer> ().material.color;
			c.a = 1f - u;
			cube.GetComponent<Renderer> ().material.color = c;
			// Fade the Letter too, just not as  much
			// On second though, do it just as much
			c = letter.color;
			//c.a = 1f - (u * 0.5f);
			c.a = 1f - u;
			letter.color = c;
		}
	}

	// This SetType() differs from those on Weapon and Projectile
	public void SetType (WeaponType wt) {
		// Grab the WeaponDefinition from Main
		WeaponDefinition def = Main.GetWeaponDefinition(wt);
		// Set the color of the Cube child
		cube.GetComponent<Renderer>().material.color = def.color;
		// letter.color = def.color // We could colorize the letter, too
		letter.text = def.letter; // Set the letter that is shown
		type = wt; // Finally, actually set the type
	}

	public void AbsorbedBy (GameObject target) {
		// We could make this fancier, but I'll get to that later...
		Destroy (gameObject);

		if (namePrefab != null) {
			GameObject n = Instantiate (namePrefab, target.transform.position, Quaternion.identity) as GameObject;
			n.GetComponent<Announcement> ().setAttributes (Main.GetWeaponDefinition(this.type).Name, Main.GetWeaponDefinition(this.type).color);
		}
	}

	void CheckOffscreen() {
		if (Utils.ScreenBoundsCheck (cube.GetComponent<Collider> ().bounds, BoundsTest.offScreen) != Vector3.zero) {
			Destroy (this.gameObject);
		}
	}
}

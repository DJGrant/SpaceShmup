﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	[SerializeField]
	private WeaponType _type;

	public float hitDamage = 1f;

	// This public property masks the field _type and takes aciton when set;
	public WeaponType type {
		get {
			return (_type);
		}
		set {
			SetType (value);
		}
	}

	void Awake () {
		// Test to see whether this has passed off screen every 2 seconds
		InvokeRepeating ("CheckOffscreen", 2f, 2f);
	}

	public void SetType (WeaponType eType) {
		// Set the _type
		_type = eType;
		WeaponDefinition def = Main.GetWeaponDefinition (_type);
		GetComponent<Renderer>().material.color = def.projectileColor;
	}

	void CheckOffscreen () {
		if (Utils.ScreenBoundsCheck (GetComponent<Collider> ().bounds, BoundsTest.offScreen) != Vector3.zero) {
			Destroy (this.gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		if (gameObject.tag == "ProjectileHero")
			hitDamage = Main.W_DEFS [this.type].damageOnHit;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision col) {
		if (LayerMask.LayerToName(col.gameObject.layer) == "Default" || LayerMask.LayerToName(col.gameObject.layer) == "Terrain") {
			Destroy (this.gameObject);
		}
	}
}

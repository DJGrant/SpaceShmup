﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinyShip : MonoBehaviour {

	public enum cannonModes
	{
		closed,
		open
	};

	public cannonModes cannonMode;

	public float speed = 10f;
	public float fireRate = 0.3f;
	public float health = 100;

	public float contactDamage;
	public GameObject explosionPrefab;

	[HideInInspector]
	public Vector3[] points; // Stores the p0 and p1 for interpolation
	[HideInInspector]
	public float timeStart; // Birth time for this Enemy_4
	//[HideInInspector]
	public float duration = 2; // Duration of movement

	[HideInInspector]
	public int showDamageForFrames;
	[HideInInspector]
	public Color[] originalColors;
	[HideInInspector]
	public Material[] materials;
	[HideInInspector]
	public int remainingDamageFrames = 0;

	[HideInInspector]
	public Bounds bounds;
	[HideInInspector]
	public Vector3 boundsCenterOffset;

	public GameObject smallProjectilePrefab;
	public GameObject bigProjectilePrefab;
	public Color projectileColor;
	public float smallProjectileDamage = 1;
	public float bigProjectileDamage = 3;

	public GameObject escapePodPrefab;
	public Color podWallColor;

	public AudioClip smallShotSound;
	public AudioClip bigShotSound;
	public AudioClip hitSound;
	//public AudioClip ineffectiveHitSound;
	public AudioClip wreckedSound;
	public AudioClip explosionSound;
	public AudioClip openCannonsSound;
	public AudioClip closeCannonsSound;

	float initialHealth;
	public DialogInstance[] DamagedDialog1;
	public DialogInstance[] DamagedDialog2;
	public DialogInstance[] WreckedDialog;
	bool damaged1 = false;
	bool damaged2 = false;
	bool doneFor = false; // About to explode;

	Collider CannonL, CannonR;

	Transform[] Guns;

	// Use this for initialization
	void Start () {
		initialHealth = health;

		cannonMode = cannonModes.closed;

		CannonL = transform.Find ("SpinyCannon_L").GetComponent<Collider> ();
		CannonR = transform.Find ("SpinyCannon_R").GetComponent<Collider> ();

		Guns = new Transform[4];
		Guns[0] = transform.Find ("SpinyShipHull/ShipGun_L").GetComponent<Transform> ();
		Guns[1] = transform.Find ("SpinyShipHull/ShipGun_R").GetComponent<Transform> ();
		Guns[2] = transform.Find ("SpinyWing_L/WingGun_L").GetComponent<Transform> ();
		Guns[3] = transform.Find ("SpinyWing_R/WingGun_R").GetComponent<Transform> ();

		showDamageForFrames = 5;

		materials = Utils.GetAllMaterials (gameObject);
		originalColors = new Color[materials.Length];
		for (int i = 0; i < materials.Length; i++) {
			originalColors[i] = materials[i].color;
		}

		points = new Vector3[2];
		// There is already an initial position chosen by Main.SpawnEnemy90
		// so add it to the points as the initial p0 and p1
		points [0] = transform.position;
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = transform.position; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		InitMovement ();

		InvokeRepeating ("CheckOffscreen", 0f, 2f);

		InvokeRepeating ("fireSmallProjectiles", 5f, 2f);
		InvokeRepeating ("fireBigProjectiles", 6f, 3f);

		InvokeRepeating ("swapCannonModes", 6f, 6f);
	}
	
	// Update is called once per frame
	void Update () {
		if (doneFor == false)
			Move();

		if (remainingDamageFrames > 0) {
			remainingDamageFrames--;
			if (remainingDamageFrames <= 0) {
				UnShowDamage();
			}
		}
	}

	void InitMovement () {
		// Pick a new point to move to that is on screen
		Vector3 p1 = Vector3.zero;
		float esp = 8;
		Bounds cBounds = Utils.camBounds;
		p1.x = Random.Range (cBounds.min.x + esp, cBounds.max.x - esp);
		p1.y = Random.Range (20 + esp, cBounds.max.y - esp);
		//p1.y = Random.Range (cBounds.min.y + esp, cBounds.max.y - esp);

		//points [0] = points [1]; // Shift points[1] to points[0]
		points [0] = transform.position;
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = p1; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		duration = (points[1] - points[0]).magnitude / 10f;

		// Reset the time
		timeStart = Time.time;
	}

	public void Move () {
		// Linear interpolation

		float u = (Time.time - timeStart) / duration;
		if (u >= 1) { // if u >= 1...
			InitMovement(); /// then initialize movement to a new point
			u = 0;
		}

		u = 1 - Mathf.Pow (1 - u, 2); // Apply Ease Out easing to u

		pos = (1 - u) * points [0] + u * points [1]; // Simple linear interpolation
	}

	public Vector3 pos {
		get {
			return (this.transform.position);
		}
		set {
			this.transform.position = value;
		}
	}

	void CheckOffscreen() {
		// If bounds are still their default value...
		if (bounds.size == Vector3.zero) {
			// then set them
			bounds = Utils.CombineBoundsOfChildren(this.gameObject);
			// Also find the difference between bounds.center and transform.position
			boundsCenterOffset = bounds.center - transform.position;
		}

		// Every time, update the bounds to the current position
		bounds.center = transform.position + boundsCenterOffset;
		//Check to see whether the bounds are completely offscreen
		Vector3 off = Utils.ScreenBoundsCheck(bounds, BoundsTest.offScreen);
		if (off != Vector3.zero) {
			// if this enemy has gone off the bottom edge of the screen
			if (off.y < 0) {
				// then destroy it
				//Destroy (this.gameObject);
				// Actually, it's a boss, so don't
			}
		}
	}

	void OnCollisionEnter (Collision col) {
		GameObject other = col.gameObject;
		Debug.Log ("Enemy hit by " + other.gameObject.name);
		switch (other.tag) {
		case "Hero":
			other.GetComponent<Hero>().changeShieldLevel( -contactDamage );
			break;

		case "ProjectileHero":
			Projectile p = other.GetComponent<Projectile> ();



			Collider colHit = col.contacts [0].thisCollider; // If it didn't get shot in the cannons
			//Debug.Log ("Spiny Collider: " + colHit.gameObject.name);

			if ((colHit != CannonL) && (colHit != CannonR)) {
				Destroy (other);
				break;
			}

			// Enemies don't take damage unless they're on the screen
			// This stops the player from shooting them before they are visible
			bounds.center = transform.position + boundsCenterOffset;
			if (bounds.extents == Vector3.zero || Utils.ScreenBoundsCheck (bounds, BoundsTest.offScreen) != Vector3.zero) {
				Destroy (other);
				break;
			}
			// Hurt this enemy
			ShowDamage ();
			Main.S.makeSound (hitSound);
			// Get the damage amount from the Projectile.type and Main.W_DEFS

			health -= Main.W_DEFS [p.type].damageOnHit;

			if ( health < (initialHealth/3)*2 && damaged1 == false) {
				DialogBox.S.setDialog (DamagedDialog1);
				damaged1 = true;
			} else if ( health < (initialHealth/3) && damaged2 == false) {
				DialogBox.S.setDialog (DamagedDialog2);
				damaged2 = true;
			}

			if (health <= 0 && doneFor == false) {
				Wrecked ();
			}
			Destroy (other);
			break;
		}
	}

	void ShowDamage () {
		foreach (Material m in materials) {
			m.color = Color.red;
		}
		remainingDamageFrames = showDamageForFrames;
	}
	void UnShowDamage () {
		for (int i = 0; i < materials.Length; i++) {
			materials [i].color = originalColors [i];
		}
	}

	public void swapCannonModes() {
		if (Main.S.gameMode == Main.gameModes.playing) {
			if (cannonMode == cannonModes.open) {
				cannonMode = cannonModes.closed;
				GetComponent<Animator> ().SetBool ("CannonsOpen", false);
				Main.S.makeSound (closeCannonsSound);
			} else {
				cannonMode = cannonModes.open;
				GetComponent<Animator> ().SetBool ("CannonsOpen", true);
				Main.S.makeSound (openCannonsSound);
			}
		}
	}

	public void fireSmallProjectile() {
		if (cannonMode == cannonModes.closed && doneFor == false && Main.S.gameMode == Main.gameModes.playing) {
			foreach (Transform t in Guns) {
				GameObject p = Instantiate (smallProjectilePrefab) as GameObject;

				p.transform.position = t.position;
				p.transform.parent = Weapon.PROJECTILE_ANCHOR;

				p.GetComponent<Projectile> ().hitDamage = smallProjectileDamage;

				p.GetComponent<Renderer> ().material.color = projectileColor;
				p.GetComponent<Rigidbody> ().velocity = Vector3.down * 30f;

				Main.S.makeSound (smallShotSound);
			}
		}
	}
	public void fireSmallProjectiles() {
		fireSmallProjectile ();
		Invoke ("fireSmallProjectile", 0.2f);
		Invoke ("fireSmallProjectile", 0.4f);
	}

	public void fireBigProjectile() {
		if (cannonMode == cannonModes.open && doneFor == false && Main.S.gameMode == Main.gameModes.playing) {
			GameObject p1 = Instantiate (bigProjectilePrefab) as GameObject;
			GameObject p2 = Instantiate (bigProjectilePrefab) as GameObject;

			p1.transform.position = CannonL.transform.position;
			p1.transform.parent = Weapon.PROJECTILE_ANCHOR;
			p2.transform.position = CannonR.transform.position;
			p2.transform.parent = Weapon.PROJECTILE_ANCHOR;

			p1.GetComponent<Projectile> ().hitDamage = bigProjectileDamage;
			p2.GetComponent<Projectile> ().hitDamage = bigProjectileDamage;

			p1.GetComponent<Renderer> ().material.color = projectileColor;
			p1.GetComponent<Rigidbody> ().velocity = Vector3.down * 25f;
			p2.GetComponent<Renderer> ().material.color = projectileColor;
			p2.GetComponent<Rigidbody> ().velocity = Vector3.down * 25f;

			Main.S.makeSound (bigShotSound);
		}
	}
	public void fireBigProjectiles() {
		fireBigProjectile ();
		Invoke ("fireBigProjectile", 0.4f);
		Invoke ("fireBigProjectile", 0.8f);
	}

	public void Wrecked() {
		GetComponent<Animator> ().SetTrigger ("Wrecked");
		doneFor = true;

		Main.S.makeSound (wreckedSound);
		Invoke ("Explode", 1f);

		DialogBox.S.setDialog (WreckedDialog);
	}

	public void Explode() {
		// Tell the Main singleton that this ship has been destroyed
		//Main.S.EnemyDestroyed (this);

		// Destroy this Enemy
		Destroy (this.gameObject);

		Main.S.makeSound (explosionSound);

		if (explosionPrefab != null)
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);

		if (escapePodPrefab != null) {
			GameObject pod = Instantiate (escapePodPrefab, transform.position, Quaternion.identity) as GameObject;
			pod.GetComponent<EscapePod> ().windowColor = projectileColor;
			pod.GetComponent<EscapePod> ().wallColor = podWallColor;
		}
	}
}

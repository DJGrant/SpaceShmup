﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TendrilsShip : MonoBehaviour {

	public float speed = 10f;
	public float fireRate = 0.3f;
	public float health = 100;

	public float contactDamage;
	public GameObject explosionPrefab;

	[HideInInspector]
	public Vector3[] points; // Stores the p0 and p1 for interpolation
	[HideInInspector]
	public float timeStart; // Birth time for this Enemy_4
	//[HideInInspector]
	public float duration = 2; // Duration of movement

	[HideInInspector]
	public int showDamageForFrames;
	[HideInInspector]
	public Color[] originalColors;
	[HideInInspector]
	public Material[] materials;
	[HideInInspector]
	public int remainingDamageFrames = 0;

	[HideInInspector]
	public Bounds bounds;
	[HideInInspector]
	public Vector3 boundsCenterOffset;

	public float rotPerSecond;
	GameObject WrenchWheel;

	public GameObject escapePodPrefab;
	public Color podWindowColor;
	public Color podWallColor;

	public AudioClip hitSound;
	public AudioClip wreckedSound;
	public AudioClip explosionSound;

	float initialHealth;
	public DialogInstance[] DamagedDialog1;
	public DialogInstance[] DamagedDialog2;
	public DialogInstance[] WreckedDialog;
	bool damaged1 = false;
	bool damaged2 = false;
	bool doneFor = false; // About to explode;

	// Use this for initialization
	void Start () {
		initialHealth = health;

		showDamageForFrames = 5;

		materials = Utils.GetAllMaterials (gameObject);
		originalColors = new Color[materials.Length];
		for (int i = 0; i < materials.Length; i++) {
			originalColors[i] = materials[i].color;
		}

		points = new Vector3[2];
		// There is already an initial position chosen by Main.SpawnEnemy90
		// so add it to the points as the initial p0 and p1
		points [0] = transform.position;
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = transform.position; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		InitMovement ();

		WrenchWheel = transform.Find ("RotatingWrenchWheel").gameObject;

		InvokeRepeating ("LungeX", 5f, 3f);
		InvokeRepeating ("LungeY", 6f, 3f);

		InvokeRepeating ("CheckOffscreen", 0f, 2f);
	}

	// Update is called once per frame
	void Update () {
		WrenchWheel.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, rotPerSecond)* Time.time * 100);

		if (doneFor == false)
			Move();

		if (remainingDamageFrames > 0) {
			remainingDamageFrames--;
			if (remainingDamageFrames <= 0) {
				UnShowDamage();
			}
		}
	}

	void InitMovement () {
		// Pick a new point to move to that is on screen
		Vector3 p1 = Vector3.zero;
		float esp = 8;
		Bounds cBounds = Utils.camBounds;

		if (pos.x < -20)
			p1.x = Random.Range (0, cBounds.max.x - esp);
		else if (pos.x > 20)
			p1.x = Random.Range (cBounds.min.x + esp, 0);
		else
			p1.x = Random.Range (cBounds.min.x + esp, cBounds.max.x - esp);

		if (pos.y < -20)
			p1.y = Random.Range (0, cBounds.max.y - esp);
		else if (pos.x > 20)
			p1.y = Random.Range (cBounds.min.y + esp, 0);
		else
			p1.y = Random.Range (cBounds.min.y + esp, cBounds.max.y - esp);
		//p1.x = Random.Range (cBounds.min.x + esp, cBounds.max.x - esp);
		//p1.y = Random.Range (cBounds.min.y + esp, cBounds.max.y - esp);


		//points [0] = points [1]; // Shift points[1] to points[0]
		points [0] = transform.position;
		if (Main.S.gameMode == Main.gameModes.playing)
			points [1] = p1; // Add p1 as points[1]
		else
			points [1] = new Vector3 (0f, 24f ,0f);

		duration = (points[1] - points[0]).magnitude / 20f;

		// Reset the time
		timeStart = Time.time;
	}

	public void Move () {
		// Linear interpolation

		float u = (Time.time - timeStart) / duration;
		if (u >= 1) { // if u >= 1...
			InitMovement(); /// then initialize movement to a new point
			u = 0;
		}

		u = 1 - Mathf.Pow (1 - u, 2); // Apply Ease Out easing to u

		pos = (1 - u) * points [0] + u * points [1]; // Simple linear interpolation
	}

	public Vector3 pos {
		get {
			return (this.transform.position);
		}
		set {
			this.transform.position = value;
		}
	}

	void CheckOffscreen() {
		// If bounds are still their default value...
		if (bounds.size == Vector3.zero) {
			// then set them
			bounds = Utils.CombineBoundsOfChildren(this.gameObject);
			// Also find the difference between bounds.center and transform.position
			boundsCenterOffset = bounds.center - transform.position;
		}

		// Every time, update the bounds to the current position
		bounds.center = transform.position + boundsCenterOffset;
		//Check to see whether the bounds are completely offscreen
		Vector3 off = Utils.ScreenBoundsCheck(bounds, BoundsTest.offScreen);
		if (off != Vector3.zero) {
			// if this enemy has gone off the bottom edge of the screen
			if (off.y < 0) {
				// then destroy it
				//Destroy (this.gameObject);
				// Actually, it's a boss, so don't
			}
		}
	}

	void OnCollisionEnter (Collision col) {
		GameObject other = col.gameObject;
		Debug.Log ("Enemy hit by " + other.gameObject.name);
		switch (other.tag) {
		case "Hero":
			other.GetComponent<Hero>().changeShieldLevel( -contactDamage );
			break;

		case "ProjectileHero":
			Projectile p = other.GetComponent<Projectile> ();
			// Enemies don't take damage unless they're on the screen
			// This stops the player from shooting them before they are visible
			bounds.center = transform.position + boundsCenterOffset;
			//if (bounds.extents == Vector3.zero || Utils.ScreenBoundsCheck (bounds, BoundsTest.offScreen) != Vector3.zero) {
			//	Destroy (other);
			//	break;
			//}
			// Hurt this enemy
			ShowDamage ();
			// Get the damage amount from the Projectile.type and Main.W_DEFS
			health -= Main.W_DEFS [p.type].damageOnHit;
			Main.S.makeSound (hitSound);

			if ( health < (initialHealth/3)*2 && damaged1 == false) {
				DialogBox.S.setDialog (DamagedDialog1);
				damaged1 = true;
			} else if ( health < (initialHealth/3) && damaged2 == false) {
				DialogBox.S.setDialog (DamagedDialog2);
				damaged2 = true;
			}

			if (health <= 0 && doneFor == false) {
				Wrecked ();
			}
			Destroy (other);
			break;
		}
	}

	void ShowDamage () {
		foreach (Material m in materials) {
			m.color = Color.red;
		}
		remainingDamageFrames = showDamageForFrames;
	}
	void UnShowDamage () {
		for (int i = 0; i < materials.Length; i++) {
			materials [i].color = originalColors [i];
		}
	}


	void LungeX () {
		if (Main.S.gameMode == Main.gameModes.playing)
			GetComponent<Animator> ().SetTrigger("LungeX");
	}
	void LungeY () {
		if (Main.S.gameMode == Main.gameModes.playing)
			GetComponent<Animator> ().SetTrigger("LungeY");
	}

	public void Wrecked() {
		GetComponent<Animator> ().SetTrigger ("Wrecked");
		doneFor = true;

		Main.S.makeSound (wreckedSound);
		Invoke ("Explode", 0.5f);

		DialogBox.S.setDialog (WreckedDialog);
	}

	public void Explode() {
		// Tell the Main singleton that this ship has been destroyed
		//Main.S.EnemyDestroyed (this);

		// Destroy this Enemy
		Destroy (this.gameObject);

		Main.S.makeSound (explosionSound);

		if (explosionPrefab != null)
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);

		if (escapePodPrefab != null) {
			GameObject pod = Instantiate (escapePodPrefab, transform.position, Quaternion.identity) as GameObject;
			pod.GetComponent<EscapePod> ().windowColor = podWindowColor;
			pod.GetComponent<EscapePod> ().wallColor = podWallColor;
		}
	}
}

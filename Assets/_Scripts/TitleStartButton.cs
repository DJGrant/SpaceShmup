﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleStartButton : MonoBehaviour {

	public AudioClip startSound;
	bool started = false;

	void Update() {
		if (!started && (Input.GetAxisRaw ("Fire1") == 1f || Input.GetMouseButtonDown (0))) {
			started = true;
			startGame ();
		}
	}

	public void startGame() {
		if (startSound != null)
			Main.S.makeSound (startSound);
		GetComponent<Animator> ().SetTrigger ("PressedStart");
		Main.S.loadNextScene ();
	}

}

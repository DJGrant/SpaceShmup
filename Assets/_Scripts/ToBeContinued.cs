﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToBeContinued : MonoBehaviour {

	public DialogInstance[] dialog;

	void Awake () {
		
	}

	// Use this for initialization
	void Start () {
		Main.S.gameMode = Main.gameModes.cutscene;

		DialogBox.S.setDialog (dialog);

		endTheGame ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void endTheGame () {
		if (Main.S.gameMode == Main.gameModes.playing) {
			GetComponent<Animator> ().SetTrigger ("BeContinued");

			Main.S.Invoke ("loadNextScene", 7f);
		} else {
			Invoke ("endTheGame", 2f);
		}
	}
}

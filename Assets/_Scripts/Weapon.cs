﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enum of weapon types
public enum WeaponType {
	none,
	extraGun,
	blaster,
	spread,
	shield
}

[System.Serializable]
public class WeaponDefinition {
	public WeaponType type = WeaponType.none;
	public string Name; // The nice name
	public string letter; // Letter on the PowerUp box
	public Color color; // Color of collar and PowerUp box
	public GameObject projectilePrefab;
	public Color projectileColor = Color.white;
	public float damageOnHit = 0f;
	public float continuousDamage = 0f; // Damage per second (Laser)
	public float delayBetweenShots;
	public float velocity = 20f;
}

// Note: The above stuff is set in the class Main

public class Weapon : MonoBehaviour {

	static public Transform PROJECTILE_ANCHOR;

	[SerializeField]
	private WeaponType _type = WeaponType.blaster;
	public WeaponDefinition def;
	public GameObject collar;
	public float lastShot; // Time last shot was fired

	void Awake() {
		collar = transform.Find ("Collar").gameObject;
	}

	// Use this for initialization
	void Start () {
		// Call SetType() properly for default _type
		SetType(_type);

		if (PROJECTILE_ANCHOR == null) {
			GameObject obj = new GameObject ("_Projectile_Anchor");
			PROJECTILE_ANCHOR = obj.transform;
		}
		// Find the fireDelegate of the parent
		//GameObject parentObj = transform.parent.gameObject;
		GameObject parentObj = Utils.FindTaggedParent(this.gameObject);
		//Debug.Log (parentObj.name);
		if (parentObj.tag == "Hero") {
			Debug.Log ("Parent: " + parentObj.name);
			//Debug.Log ("S: " + Hero.S.name);
			//Debug.Log ("fireDelegate: " + Hero.S.fireDelegate);
			Hero.S.fireDelegate += Fire;
		}
	}

	public WeaponType type {
		get { return( _type ); }
		set { SetType (value); }
	}

	public void SetType(WeaponType wt) {
		_type = wt;
		if (type == WeaponType.none) {
			this.gameObject.SetActive (false);
			return;
		} else {
			this.gameObject.SetActive (true);
		}
		def = Main.GetWeaponDefinition (_type);
		collar.GetComponent<Renderer> ().material.color = def.color;
		lastShot = 0; // You can always fire immediately after _type is set
	}

	public void Fire() {
		// if this.gameObject is inactive, return
		if (!gameObject.activeInHierarchy) return;
		// If it hasn't been enough time between shots, return
		if (Time.time - lastShot < def.delayBetweenShots) {
			return;
		}
		Projectile p;
		switch (type) {
		case WeaponType.blaster:
			p = MakeProjectile ();
			p.GetComponent<Rigidbody> ().velocity = Vector3.up * def.velocity;
			break;

		case WeaponType.spread:
			p = MakeProjectile ();
			p.GetComponent<Rigidbody> ().velocity = Vector3.up * def.velocity;
			p = MakeProjectile ();
			p.GetComponent<Rigidbody> ().velocity = new Vector3 (-0.2f, 0.9f, 0f) * def.velocity;
			p = MakeProjectile ();
			p.GetComponent<Rigidbody> ().velocity = new Vector3 (0.2f, 0.9f, 0f) * def.velocity;
			break;
		}

	}

	public Projectile MakeProjectile() {
		GameObject obj = Instantiate (def.projectilePrefab) as GameObject;
		Projectile p = obj.GetComponent<Projectile> ();
		if (Utils.FindTaggedParent(this.gameObject).tag == "Hero") {
			obj.tag = "ProjectileHero";
			obj.layer = LayerMask.NameToLayer ("ProjectileHero");
			p.type = type;
			lastShot = Time.time;
		} else {
			obj.tag = "ProjectileEnemy";
			obj.layer = LayerMask.NameToLayer ("ProjectileEnemy");
		}
		obj.transform.position = collar.transform.position;
		obj.transform.parent = PROJECTILE_ANCHOR;
		return (p);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
